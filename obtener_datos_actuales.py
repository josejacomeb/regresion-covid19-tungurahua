"""
Programa para obtener los datos actualizados de COVID en Tungurahua
Datos obtenidos del repositorio https://github.com/andrab/ecuacovid
Crédito a los autores
"""

import os
import requests
import sys

directorio_padre = "https://raw.githubusercontent.com/andrab/ecuacovid/master/"

datos_descargar_tungurahua = [
    "datos_crudos/camas/provincias.csv",
    "datos_crudos/defunciones/provincias.csv",
    "datos_crudos/muertes/provincias.csv",
    "datos_crudos/positivas/provincias.csv" ,
    "datos_crudos/vacunometro/provincias.csv"
]

for archivo_descargar in datos_descargar_tungurahua:
    ruta_archivo = os.path.dirname(archivo_descargar)
    if not os.path.exists(ruta_archivo):
        carpeta_actual = os.getcwd()
        ruta_total = os.path.join(carpeta_actual, ruta_archivo)
        nombre_primera_carpeta = ruta_archivo.split("/")[0]
        if not os.path.exists(nombre_primera_carpeta):
            print("Creando carpeta {}".format(nombre_primera_carpeta))
            os.mkdir(nombre_primera_carpeta)
        os.mkdir(ruta_total)
        print("Creando carpeta en: {}".format(ruta_total))
    url_archivo_descargar = os.path.join(directorio_padre, archivo_descargar)
    r = requests.get(url_archivo_descargar, allow_redirects=True)

    tamano_request = sys.getsizeof(r.content)
    tamano_archivo_descargado = 0
    if os.path.isfile(archivo_descargar):
        tamano_archivo_descargado = os.path.getsize(archivo_descargar)
    if tamano_request != tamano_archivo_descargado:
        print("Archivo {} ha cambiado, actualizando con el repositorio ".format(archivo_descargar))
        with open(archivo_descargar, 'wb') as escribir_archivo:
            escribir_archivo.write(r.content)
            escribir_archivo.truncate()
