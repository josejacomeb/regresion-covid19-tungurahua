# RegresionCOVID19 Tungurahua
Programa creado para realizar una regresión multivariable con los datos de COVID-19
Fuentes de datos:

- 1. [Ecuacovid](https://github.com/andrab/ecuacovid/tree/master/datos_crudos)
- 2. [COVID-19 Community Mobility Reports](https://www.google.com/covid19/mobility/)

## Instalación
Instalar [Python 3](https://www.python.org/downloads/release/python-380/) para la distribución requerida.  
Usar el terminal para entrar a la carpeta descargada, luego, instalar las dependencias con el siguiente comando:

```bash
pip install -r requirements.txt
```

## Ejecución del Entorno Jupyter Notebook

El la misma carpeta del proyecto, 

## Fuente de datos

| Referencia | Variables dependientes      | Variables independientes      | Métricas            |
|------------|-----------------------------|-------------------------------|---------------------|
| [1]        | Casos confirmados           | Historia de viaje y contactos |                     |
| [2]        | Histórico Casos confirmados |                               |                     |
| [3]        | Contagios y acumulados      |                               | Porcentaje de error |
| [4]        | Casos confirmados, muertes y recuperados |                               | RMSLE               |

## Referencias 
- [1] [Predictive modelling of COVID-19 confirmed cases in Nigeria](https://www.sciencedirect.com/science/article/pii/S2468042720300336)
- [2] [Count regression models for COVID-19](https://www.sciencedirect.com/science/article/abs/pii/S0378437120307743)
- [3] [Importancia de los modelos de regresión no lineales en la interpretación de datos de la COVID-19 en Colombia](http://scielo.sld.cu/scielo.php?script=sci_abstract&pid=S1729-519X2020000400014&lng=es&nrm=iso&tlng=en)
- [4] [SEIR and Regression Model based COVID-19 outbreak predictions in India](https://arxiv.org/abs/2004.00958)
